#pragma once

#include <map>
#include <set>
#include <vector>

struct Player;

struct Zone {
	Player* owner;
	std::set<Zone*> neighbours;
	unsigned int production;
	std::map<Player*, int> numPods;
};

typedef std::vector<Zone> Map;

struct Movement {
	std::size_t multiplier;
	Map::iterator from, to;
};

struct Player {
	unsigned int id;
	Zone* base;
	unsigned int numPlatinums;
	std::vector<Movement> calcMovements(Map, Player& me, Player& opponent) { return std::vector<Movement>(0); };
};
