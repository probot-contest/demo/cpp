#include "interface.h"
#include "common.h"

#include "main.h"

int main(int argc, char** argv) {
	Interface interface;
	return interface.run();
}
