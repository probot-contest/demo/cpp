#include "common.h"
#include <vector>

#include "interface.h"

int Interface::run(Map& map, Player& me, Player& opponent, std::istream& in, std::ostream& out) {
	Player *firstPlayer, *secondPlayer;

	// First round only state updates: Game constants
	{
		std::size_t n, m;
		unsigned int myBase, opponentsBase;

		// First row
		in >> me.id >> n >> myBase >> opponentsBase >> m;

		opponent.id = 1 - me.id;
		if (me.id == 0) {
			firstPlayer = &me;
			secondPlayer = &opponent;
		} else { // (me.id == 1)
			firstPlayer = &opponent;
			secondPlayer = &me;
		}
		map.resize(n);

		// Second row
		for (Zone zone : map) {
			in >> zone.production;
		}

		// Starting from third row
		for (size_t i = 0; i < m; i++) {
			int a, b;

			// One row
			in >> a, b;

			map[a].neighbours.insert(&map[b]);
			map[b].neighbours.insert(&map[a]);
		}
	}

	// Regular rounds
	while (true) {
		// State updates
		{
			// First row
			in >> me.numPlatinums;

			for (Zone zone : map) {
				int owner;

				// One row
				in >> owner >> zone.numPods[firstPlayer] >> zone.numPods[secondPlayer];

				zone.owner = owner == -1 ? nullptr : ( owner == 0 ? firstPlayer : secondPlayer );
			}
		}

		if (out.eof()) return 5;
		if (out.fail()) return 4;
		if (in.eof()) return 3;
		if (in.fail()) return 2;

		// Movement
		{
			const std::vector<Movement> movements = me.calcMovements(map, me, opponent);
			
			for (Movement movement : movements) {
				out << movement.multiplier << " " << movement.from - map.begin() << " " << movement.to - map.begin() << " ";
			}
			out << -1 << std::endl;
		}
	}
}
