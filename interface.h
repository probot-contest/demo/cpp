#pragma once

#include <iostream>
#include "common.h"

struct Interface {
	int run(Map& map = *new Map(), Player& me = *new Player(), Player& opponent = *new Player(), std::istream& in = std::cin, std::ostream& out = std::cout);
};
